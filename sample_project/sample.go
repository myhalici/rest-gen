package main

import (
	"bitbucket.org/kpratt/rest-gen"
	"bitbucket.org/kpratt/rest-gen/rest_gen_go"
	"fmt"
	"os"
)

func main() {
	resource := rest_gen.Resource{
		Package:    "generated_sample",
		Name:       "Person",
		UrlParam:   []rest_gen.Field{},
		QueryParam: []rest_gen.Field{},
		JsonFields: []rest_gen.Field{
			{
				Name:     "first_name",
				Required: true,
				Control:  rest_gen.IMMUTABLE,
				Kind:     rest_gen.STRING,
			},
			{
				Name:     "last_name",
				Required: true,
				Control:  rest_gen.MUTABLE,
				Kind:     rest_gen.STRING,
			},
			{
				Name:    "middle_name",
				Control: rest_gen.MUTABLE,
				Kind:    rest_gen.STRING,
			},
			{
				Name:    "created_at",
				Control: rest_gen.HIDDEN,
				Kind:    rest_gen.TIMESTAMP,
			},
			{
				Name:     "DOB",
				Required: true,
				Control:  rest_gen.IMMUTABLE,
			},
			{
				Name:    "last_login",
				Control: rest_gen.READONLY,
			},
		},
	}


	err := rest_gen_go.GenerateGoModule([]rest_gen.Resource{ resource }, "./generated")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
}
