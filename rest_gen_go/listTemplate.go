package rest_gen_go

/*
@param limit int
@param offset int
@param sortby [feild]
@param other map[string]interface{}

 */

var listTemplate string = `
type List{{.Name}}Models func(limit int, offset int, sortby []string, other map[string]interface{}) ([]{{.Name}}ClientModel, error)

func (controller *{{.Name}}Controller) List{{.Name}}(f List{{.Name}}Models) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		var ok bool
		var wrongType []string = make([]string, 0, 4)
		var jsonObj map[string]interface{}

		bites, err := ioutil.ReadAll(r.Body)
		err = json.Unmarshal(bites, &jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var limit int = 50
		if _, ok = jsonObj["limit"]; ok {
			if field, ok := jsonObj["limit"].(float64); ok {
				limit = int(field)
			} else {
				wrongType = append(wrongType, "limit: int")
			}
			delete(jsonObj, "limit")
		}

		var offset int = 0
		if _, ok = jsonObj["offset"]; ok {
			if field, ok := jsonObj["offset"].(float64); ok {
				offset = int(field)
			} else {
				wrongType = append(wrongType, "offset: int")
			}
			delete(jsonObj, "offset")
		}

		var sortBy []string = []string{"id"}
		if _, ok = jsonObj["sortBy"]; ok {
			if field, ok := jsonObj["sortBy"].([]string); ok { // pretty sure i cant cast this, will need to use []interface{}
				if len(field) > 0 {
					sortBy = field
				}
			} else {
				wrongType = append(wrongType, "sortBy: [string]")
			}
			delete(jsonObj, "sortBy")
		}

		resources, err := f(limit, offset, sortBy, jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		buff := new(bytes.Buffer)
		buff.Write([]byte("["))
		for i:=0; i<len(resources); i++ {
			if i!=0 {
				buff.Write([]byte(", "))
			}
			output, err := jsonEncode{{.Name}}(resources[i])
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
			}
			buff.Write(output)
		}
		buff.Write([]byte("]"))

		_, err = w.Write([]byte(buff.String()))
		if err != nil {
			log.Printf("Failed to write response [ %s ]", err)
		}
	}
}
`