package rest_gen_go

import (
	"io"
	"bitbucket.org/kpratt/rest-gen"
	"text/template"
	"os"
	"strings"
)

func GenerateGoModule(resources []rest_gen.Resource, dir string) error {
	os.MkdirAll(dir, os.FileMode(0755))

	for _, resource := range resources {

		serverCodeFile, err := os.OpenFile(dir + "/" + strings.ToLower(resource.Name) + "_controller.go", os.O_WRONLY | os.O_TRUNC | os.O_CREATE, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer serverCodeFile.Close()

		clientCodeFile, err := os.OpenFile(dir + "/" + strings.ToLower(resource.Name) + "_client.go", os.O_WRONLY | os.O_TRUNC | os.O_CREATE, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer clientCodeFile.Close()

		testFile, err := os.OpenFile(dir + "/" + strings.ToLower(resource.Name) + "_test.go", os.O_WRONLY | os.O_TRUNC | os.O_CREATE, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer testFile.Close()

		err = GenerateGoCode(resource, serverCodeFile, clientCodeFile, testFile)
		if err != nil {
			return err
		}
	}
	return nil
}

func GenerateGoCode(resource rest_gen.Resource, server io.Writer, client io.Writer, test io.Writer) error {
	funcs := template.FuncMap{
		"fieldType": templateFuncFeildType,
		"assertType": templateFuncFeildAssertType,
		"constructType": templateFuncFeildConstructtType,
	}

	serverTemplates := [][]string{
		[]string{" Common Code Server template", commonTemplate},
		[]string{" HTTP GET Server template", readTemplate},
		[]string{" HTTP POST Server template", createTemplate},
		[]string{" HTTP PUT Server template", updateTemplate},
		[]string{" HTTP DELETE Server template", deleteTemplate},
		[]string{" HTTP List Server template", listTemplate},
	}
	for i := 0; i < len(serverTemplates); i++ {
		t, err := template.New(resource.Name + serverTemplates[i][0]).Funcs(funcs).Parse(serverTemplates[i][1])
		if err != nil {
			return err
		}
		err = t.Execute(server, resource)
		if err != nil {
			return err
		}
	}

	// Rest Client
	clientTemplates := [][]string{
		[]string{" Common Code Client template", clientCommonTemplate},
		[]string{" HTTP GET Client template", ""},
		[]string{" HTTP POST Client template", ""},
		[]string{" HTTP PUT Client template", ""},
		[]string{" HTTP DELETE Client template", ""},
		[]string{" HTTP List Client template", ""},
	}
	for i := 0; i < len(clientTemplates); i++ {
		t, err := template.New(resource.Name + clientTemplates[i][0]).Funcs(funcs).Parse(clientTemplates[i][1])
		if err != nil {
			return err
		}
		err = t.Execute(client, resource)
		if err != nil {
			return err
		}
	}

	// Tests
	testTemplates := [][]string{
		[]string{" Common Code Test template", testCommonTemplate},
		[]string{" HTTP GET Test template", ""},
		[]string{" HTTP POST Test template", ""},
		[]string{" HTTP PUT Test template", ""},
		[]string{" HTTP DELETE Test template", ""},
		[]string{" HTTP List Test template", ""},
	}
	for i := 0; i < len(serverTemplates); i++ {
		t, err := template.New(resource.Name + testTemplates[i][0]).Funcs(funcs).Parse(testTemplates[i][1])
		if err != nil {
			return err
		}
		err = t.Execute(test, resource)
		if err != nil {
			return err
		}
	}

	return nil
}

func templateFuncFeildType(field rest_gen.Field) string {
	if field.Required {
		switch field.Kind {
		case rest_gen.STRING: return "string"
		case rest_gen.INT: return "int"
		case rest_gen.FLOAT: return "float64"
		case rest_gen.TIMESTAMP: return "time.Time"
		}
	} else {
		switch field.Kind {
		case rest_gen.STRING: return "*string"
		case rest_gen.INT: return "*int"
		case rest_gen.FLOAT: return "*float64"
		case rest_gen.TIMESTAMP: return "*time.Time"
		}
	}
	return "<Unknown Type Specified>"
}

func templateFuncFeildAssertType(field rest_gen.Field) string {
	switch field.Kind {
	case rest_gen.STRING: return "string"
	case rest_gen.INT: return "float64"
	case rest_gen.FLOAT: return "float64"
	case rest_gen.TIMESTAMP: return "string"
	}
	return "<Unknown Type Specified>"
}

func templateFuncFeildConstructtType(field rest_gen.Field, varName string) string {
	switch field.Kind {
	case rest_gen.STRING: return "string(" + varName + ")"
	case rest_gen.INT: return "int(" + varName + ")"
	case rest_gen.FLOAT: return "float64(" + varName + ")"
	case rest_gen.TIMESTAMP: return "time.Time(" + varName + ")"
	}
	return "<Unknown Type Specified>"
}