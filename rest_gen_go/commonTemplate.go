package rest_gen_go

const commonTemplate string = `
package {{.Package}}

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"log"
	"errors"
	"bytes"
	"time"
)


type {{.Name}}Model struct {
	id             int // read-only
	{{range .JsonFields}}
		{{.Name}} {{fieldType .}}
	{{end}}
}

type {{.Name}}DefaultParams struct {
	{{range .JsonFields}}
		{{if not .Required}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}ClientModel struct {
	id             int
	{{range .JsonFields}}
		{{if not .IsHidden}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}CreateModel struct {
	{{range .JsonFields}}
		{{if and (not .IsReadOnly) (not .IsHidden)}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

type {{.Name}}UpdateModel struct {
	{{range .JsonFields}}
		{{if .IsMutable}}
			{{.Name}} {{fieldType .}}
		{{end}}
	{{end}}
}

func New{{.Name}}(params {{.Name}}CreateModel, defaults {{.Name}}DefaultParams) {{.Name}}Model {
	// for required and not read-only / hidden
	model := {{.Name}}Model{}
	{{range .JsonFields}}
		{{if and (.Required) (not .IsWritable)}}
			model.{{.Name}} = params.{{.Name}}
		{{end}}
	{{end}}

	{{range .JsonFields}}
		{{if and (not .Required) (.IsWritable) }}
			if params.{{.Name}} != nil {
				model.{{.Name}} = params.{{.Name}}
			} else {
				x := *defaults.{{.Name}}
				model.{{.Name}} = &x
			}
		{{end}}
	{{end}}
	// for optional and not read-only / hidden

	return model
}

func (model *{{.Name}}Model) UpdateWith(params {{.Name}}UpdateModel) {
	// for not read-only / immutable / hidden
	{{range .JsonFields}}
		{{if .IsMutable}}
			{{if .Required}}
				model.{{.Name}} = params.{{.Name}}
			{{else}}
				if params.{{.Name}} != nil {
					model.{{.Name}} = params.{{.Name}}
				}
			{{end}}
		{{end}}
	{{end}}
}

func (resource *{{.Name}}Model) ForClient() (visible {{.Name}}ClientModel) {
	// for not hidden
	{{range .JsonFields}}
		{{if not .IsHidden}}
			visible.{{.Name}} = resource.{{.Name}}
		{{end}}
	{{end}}
	return
}

type {{.Name}}Controller struct {

}

func jsonEncode{{.Name}}(r {{.Name}}ClientModel) ([]byte, error) {
	//return []byte(fmt.Sprintf("{\"immutableFeild\": %d, \"readOnlyFeild\": %d, \"requiredFeild\": \"%s\", \"optionalFeild\": %d}", r.immutableFeild, r.readOnlyFeild, r.requiredFeild, *r.optionalFeild)), nil
	return []byte{}, nil
}
`
