package rest_gen_go

const updateTemplate string = `

type Update{{.Name}}Model func(resource {{.Name}}UpdateModel) ({{.Name}}ClientModel, error)

func (controller *{{.Name}}Controller) Update{{.Name}}(f Update{{.Name}}Model) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		var ok bool

		bites, err := ioutil.ReadAll(r.Body)
		var resource {{.Name}}UpdateModel
		var jsonObj map[string]interface{}
		err = json.Unmarshal(bites, &jsonObj)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While decodeing an {{.Name}} [ %s ] raw data was [ %s ] ", err, string(bites))
		}

		var missing []string = make([]string, 0, 4)
		// for required feilds
		{{range .JsonFields}}
			{{if and .IsMutable .Required}}
				if _, ok = jsonObj["{{.Name}}"]; !ok {
					missing = append(missing, "{{.Name}}")
				}
			{{end}}
		{{end}}

		// TODO custom error type
		if len(missing) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Feild(s) were missing [")
			for i := 0; i < len(missing); i++ {
				buff.WriteString(missing[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		var wrongType []string = make([]string, 0, 4)

		// for all required feilds
		{{range .JsonFields}}
			{{if and .Required .IsMutable}}
			if field, ok := jsonObj["{{.Name}}"].({{assertType .}}); ok {
					x := {{constructType . "field"}}
					resource.{{.Name}} = x
			} else {
				wrongType = append(wrongType, "{{.Name}}: {{fieldType .}}")
			}
			{{end}}
		{{end}}

		// for all optional feilds
		{{range .JsonFields}}
			{{if and (not .Required) .IsMutable}}
				if _, ok = jsonObj["{{.Name}}"]; ok {
					if field, ok := jsonObj["{{.Name}}"].({{assertType .}}); ok {
						x := {{constructType . "field"}}
						resource.{{.Name}} = &x
					} else {
						wrongType = append(wrongType, "{{.Name}}: {{fieldType .}}")
					}
				}
			{{end}}
		{{end}}

		// TODO custom error type
		if len(wrongType) > 0 {
			buff := new(bytes.Buffer)
			buff.WriteString("Type error. Feilds should have type: [")
			for i := 0; i < len(wrongType); i++ {
				buff.WriteString(wrongType[i])
				buff.WriteString(", ")
			}
			buff.WriteString("]")
			err = errors.New(buff.String())

			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err)))
		}

		visible, err := f(resource)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an {{.Name}} post create [ %s ]", err)
		}

		output, err := jsonEncode{{.Name}}(visible)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("While encoding an {{.Name}} post create [ %s ]", err)
		}

		_, err = w.Write(output)
		if err != nil {
			log.Printf("Failed to write create response [ %s ]", err)
		}
	}
}
`